package com.egs.sergey.mytestapp.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Table(name = "PointDetail")
public class PointDetail extends Model implements Serializable {

    @SerializedName("id")
    @Column(name = "Poi_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private Long poiId;
    @Column(name = "Title")
    private String title;
    @Column(name = "Address")
    private String address;
    @Column(name = "Transport")
    private String transport;
    @Column(name = "Email")
    private String email;
    @Column(name = "Geo_coordinates")
    private String geocoordinates;
    @Column(name = "Description")
    private String description;
    @Column(name = "Phone")
    private String phone;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGeocoordinates() {
        return geocoordinates;
    }

    public void setGeocoordinates(String geocoordinates) {
        this.geocoordinates = geocoordinates;
    }

    public PointDetail() {
        super();
    }

    public Long getPoiId() {
        return poiId;
    }

    public void setPoiId(Long poiId) {
        this.poiId = poiId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
