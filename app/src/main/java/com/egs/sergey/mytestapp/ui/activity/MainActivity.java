package com.egs.sergey.mytestapp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.egs.sergey.mytestapp.R;
import com.egs.sergey.mytestapp.bus.AndroidBus;
import com.egs.sergey.mytestapp.model.Point;
import com.egs.sergey.mytestapp.event.LoadPointDetailEvent;
import com.egs.sergey.mytestapp.event.PointDetailLoadedEvent;
import com.egs.sergey.mytestapp.event.LoadPointsEvent;
import com.egs.sergey.mytestapp.event.PointsLoadedEvent;
import com.egs.sergey.mytestapp.ui.adapter.PointsAdapter;
import com.egs.sergey.mytestapp.util.Constants;
import com.egs.sergey.mytestapp.util.ViewHelper;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener, PointsAdapter.OnPointClickedListener {

    private static boolean IS_DATA_LOADED = false;
    private static final String BUNDLE_STATE = "BUNDLE_STATE";

    private PointsAdapter adapter;
    private List<Point> points;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private FrameLayout progressFrame;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(toolbar);
        setToolbarTitle(getString(R.string.main_activity_toolbar_title));

        if (savedInstanceState != null) {
            IS_DATA_LOADED = savedInstanceState.getBoolean(BUNDLE_STATE);
        } else {
            IS_DATA_LOADED = false;
        }
        initView();
    }

    private void initView() {
        progressFrame = (FrameLayout) findViewById(R.id.activity_main_progressFrame);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.activity_main_coordinatorLayout);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);
        recyclerView = (RecyclerView) findViewById(R.id.activity_main_recyclerView);

        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        points = new ArrayList<>();
        adapter = new PointsAdapter(this, points, this);

        recyclerView.setAdapter(adapter);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(BUNDLE_STATE, IS_DATA_LOADED);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AndroidBus.getInstance().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AndroidBus.getInstance().register(this);
        if (!IS_DATA_LOADED) {
            progressFrame.setVisibility(View.VISIBLE);
            AndroidBus.getInstance().post(new LoadPointsEvent());
        }
    }

    // Event will be called even in case of failure
    @Subscribe
    public void onPointsLoadedEvent(PointsLoadedEvent event) {
        progressFrame.setVisibility(View.GONE);
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
        if (event.isError() && event.getPoints().size() == 0) {
            ViewHelper.showSnackbar(this, coordinatorLayout, event.getErrorMessage(), true);
            return;
        }
        points.clear();
        points.addAll(event.getPoints());
        // To be able filter newly received data
        points = event.getPoints();
        adapter.notifyDataSetChanged();
        IS_DATA_LOADED = true;
    }

    @Subscribe
    public void onPointDetailLoadedEvent(PointDetailLoadedEvent event) {
        if (event.isError() && event.getPointDetail() == null) {
            progressFrame.setVisibility(View.GONE);
            ViewHelper.showSnackbar(this, coordinatorLayout, event.getErrorMessage(), true);
            return;
        }
        Intent intent = new Intent(this, DetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.EXTRA_POINT, event.getPointDetail());
        intent.putExtras(bundle);
        progressFrame.setVisibility(View.GONE);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        // Reload data
        AndroidBus.getInstance().post(new LoadPointsEvent());
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final List<Point> filteredModelList = filter(points, newText);
        adapter.animateTo(filteredModelList);
        recyclerView.scrollToPosition(0);
        return true;
    }

    /**
     *
     * @param models the list of points
     * @param query criteria by which points will be filtered
     * @return filtered list
     */
    private List<Point> filter(List<Point> models, String query) {
        query = query.toLowerCase();

        final List<Point> filteredModelList = new ArrayList<>();
        for (Point model : models) {
            final String text = model.getTitle().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public void onPointClicked(Point point) {
        progressFrame.setVisibility(View.VISIBLE);
        AndroidBus.getInstance().post(new LoadPointDetailEvent(point.getPoiId()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
