package com.egs.sergey.mytestapp.event;

import com.egs.sergey.mytestapp.model.Point;

import java.util.List;

public class PointsLoadedEvent {

    private List<Point> points;
    private boolean isError;
    private String errorMessage;

    public PointsLoadedEvent(List<Point> points) {
        this.points = points;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public boolean isError() {
        return isError;
    }

    public void setIsError(boolean isError) {
        this.isError = isError;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
