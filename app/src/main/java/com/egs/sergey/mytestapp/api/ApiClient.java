package com.egs.sergey.mytestapp.api;

import com.egs.sergey.mytestapp.model.PointDetail;
import com.egs.sergey.mytestapp.model.PointList;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

public interface ApiClient {

    @GET("/points")
    void getPoints(Callback<PointList> callback);

    @GET("/points/{id}")
    void getDetailPoint(@Path("id") Long id, Callback<PointDetail> callback);

}
