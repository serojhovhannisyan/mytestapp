package com.egs.sergey.mytestapp.event;

public class LoadPointDetailEvent {

    private Long id;

    public LoadPointDetailEvent(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
