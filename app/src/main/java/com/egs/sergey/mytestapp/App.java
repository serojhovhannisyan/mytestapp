package com.egs.sergey.mytestapp;

import android.app.Application;

import com.activeandroid.ActiveAndroid;
import com.egs.sergey.mytestapp.bus.AndroidBus;
import com.egs.sergey.mytestapp.service.PoiService;
import com.squareup.otto.Bus;

public class App extends Application {

    private PoiService poiService;
    private Bus bus = AndroidBus.getInstance();

    @Override
    public void onCreate() {
        super.onCreate();
        poiService = new PoiService(this, bus);
        bus.register(poiService);
        bus.register(this);
        ActiveAndroid.initialize(this);
    }

}
