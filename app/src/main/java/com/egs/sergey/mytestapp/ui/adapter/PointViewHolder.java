package com.egs.sergey.mytestapp.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.egs.sergey.mytestapp.R;

public class PointViewHolder extends RecyclerView.ViewHolder {

    public TextView title;

    public PointViewHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.list_item_point_title);
    }

}
