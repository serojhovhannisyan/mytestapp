package com.egs.sergey.mytestapp.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.egs.sergey.mytestapp.R;
import com.egs.sergey.mytestapp.model.Point;

import java.util.List;

public class PointsAdapter extends RecyclerView.Adapter<PointViewHolder> implements View.OnClickListener {

    private Context context;
    private final List<Point> points;
    private OnPointClickedListener listener;

    public PointsAdapter(Context context, List<Point> points, OnPointClickedListener listener) {
        this.context = context;
        this.points = points;
        this.listener = listener;
    }

    @Override
    public PointViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_point, parent, false);
        view.setOnClickListener(this);
        return new PointViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PointViewHolder holder, int position) {
        Point point = points.get(position);
        holder.title.setText(point.getTitle());
        holder.itemView.setTag(point);
    }

    @Override
    public int getItemCount() {
        return points.size();
    }


    public void animateTo(List<Point> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<Point> newModels) {
        for (int i = points.size() - 1; i >= 0; i--) {
            final Point model = points.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Point> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Point model = newModels.get(i);
            if (!points.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Point> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Point model = newModels.get(toPosition);
            final int fromPosition = points.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public Point removeItem(int position) {
        final Point model = points.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, Point model) {
        points.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Point model = points.remove(fromPosition);
        points.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onClick(View view) {
        if (view.getTag() instanceof Point) {
            Point point = (Point) view.getTag();
            listener.onPointClicked(point);
        }
    }

    public interface OnPointClickedListener {
        void onPointClicked(Point point);
    }
}
