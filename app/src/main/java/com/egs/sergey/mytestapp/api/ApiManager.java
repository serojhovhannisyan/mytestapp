package com.egs.sergey.mytestapp.api;

import com.egs.sergey.mytestapp.util.Constants;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

public final class ApiManager {


    private static ApiClient apiClient;

    public static ApiClient getClient() {
        if (apiClient == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constants.ENDPOINT)
                    .setClient(new OkClient(new OkHttpClient()))
                    .build();
            apiClient = restAdapter.create(ApiClient.class);
        }
        return apiClient;
    }

    private ApiManager() {}

}
