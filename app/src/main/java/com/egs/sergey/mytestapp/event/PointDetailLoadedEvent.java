package com.egs.sergey.mytestapp.event;

import com.egs.sergey.mytestapp.model.PointDetail;

public class PointDetailLoadedEvent {

    private PointDetail pointDetail;
    private boolean isError;
    private String errorMessage;

    public PointDetailLoadedEvent(PointDetail pointDetail) {
        this.pointDetail = pointDetail;
    }

    public PointDetail getPointDetail() {
        return pointDetail;
    }

    public void setPointDetail(PointDetail pointDetail) {
        this.pointDetail = pointDetail;
    }

    public boolean isError() {
        return isError;
    }

    public void setIsError(boolean isError) {
        this.isError = isError;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
