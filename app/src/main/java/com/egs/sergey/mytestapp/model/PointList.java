package com.egs.sergey.mytestapp.model;

import java.util.List;

public class PointList {

    private List<Point> list;

    public List<Point> getList() {
        return list;
    }

    public void setList(List<Point> list) {
        this.list = list;
    }
}