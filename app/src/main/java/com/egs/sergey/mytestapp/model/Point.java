package com.egs.sergey.mytestapp.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

@Table(name = "Point")
public class Point extends Model {

    @SerializedName("id")
    @Column(name = "Poi_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private Long poiId;
    @Column(name = "Title")
    private String title;
    @Column(name = "Geo_coordinates")
    private String geocoordinates;

    public Point() {
        super();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGeocoordinates() {
        return geocoordinates;
    }

    public void setGeocoordinates(String geocoordinates) {
        this.geocoordinates = geocoordinates;
    }

    public Long getPoiId() {
        return poiId;
    }

    public void setPoiId(Long poiId) {
        this.poiId = poiId;
    }

}

