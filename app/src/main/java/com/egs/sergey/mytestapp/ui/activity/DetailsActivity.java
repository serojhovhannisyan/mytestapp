package com.egs.sergey.mytestapp.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.egs.sergey.mytestapp.R;
import com.egs.sergey.mytestapp.model.PointDetail;
import com.egs.sergey.mytestapp.util.Constants;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class DetailsActivity extends BaseActivity implements OnMapReadyCallback {

    private PointDetail pointDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_details_toolbar);
        setSupportActionBar(toolbar);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initData();
        initToolbar(toolbar);
        initView(pointDetail);
    }

    private void initData() {
        if (!getIntent().hasExtra(Constants.EXTRA_POINT)) {
            throw new IllegalArgumentException("The PointDetail extra should be provided");
        }
        pointDetail = (PointDetail) getIntent().getSerializableExtra(Constants.EXTRA_POINT);
    }

    private void initToolbar(Toolbar toolbar) {
        setToolbarTitle(pointDetail.getTitle());
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initView(PointDetail pointDetail) {
        TextView addressTextView = (TextView) findViewById(R.id.activity_details_address);
        TextView transportTextView = (TextView) findViewById(R.id.activity_details_transport);
        TextView emailTextView = (TextView) findViewById(R.id.activity_details_email);
        TextView phoneTextView = (TextView) findViewById(R.id.activity_details_phone);
        TextView descriptionTextView = (TextView) findViewById(R.id.activity_details_description);

        CardView addressCardView = (CardView) findViewById(R.id.activity_details_addressCardView);
        CardView transportCardView = (CardView) findViewById(R.id.activity_details_transportCardView);
        CardView emailCardView = (CardView) findViewById(R.id.activity_details_emailCardView);
        CardView phoneCardView = (CardView) findViewById(R.id.activity_details_phoneCardView);
        CardView descriptionCardView = (CardView) findViewById(R.id.activity_details_descriptionCardView);

        addTextToView(addressTextView, addressCardView, pointDetail.getAddress());
        addTextToView(transportTextView, transportCardView, pointDetail.getTransport());
        addTextToView(emailTextView, emailCardView, pointDetail.getEmail());
        addTextToView(phoneTextView, phoneCardView, pointDetail.getPhone());
        addTextToView(descriptionTextView, descriptionCardView, pointDetail.getDescription());
    }

    private void addTextToView(TextView view, CardView linearLayout, String text) {
        if (text == null || text.isEmpty() || text.equals(Constants.UNWANTED_WORD_NULL) || text.equals(Constants.UNWANTED_WORD_UNDEFINED)) {
            linearLayout.setVisibility(View.GONE);
        } else {
            view.setText(text);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.setMyLocationEnabled(true);
        map.getUiSettings().setAllGesturesEnabled(true);
        map.getUiSettings().setCompassEnabled(true);

        LatLng target = null;
        if (pointDetail.getGeocoordinates() != null) {
            String[] coordinates = pointDetail.getGeocoordinates().split(",");
            target = new LatLng(Double.valueOf(coordinates[0]), Double.valueOf(coordinates[1]));
        }

        map.addMarker(new MarkerOptions().position(target).title(pointDetail.getAddress()));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(target)
                .zoom(3)
                .bearing(15)
                .tilt(10)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        map.animateCamera(cameraUpdate);

    }
}
