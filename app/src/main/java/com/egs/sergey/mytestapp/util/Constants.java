package com.egs.sergey.mytestapp.util;

public class Constants {

    public static final String ENDPOINT = "http://t21services.herokuapp.com";
    public static final String EXTRA_POINT = "com.egs.sergey.extra_point";
    public static final String UNWANTED_WORD_NULL = "null";
    public static final String UNWANTED_WORD_UNDEFINED = "undefined";

}
