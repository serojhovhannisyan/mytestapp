package com.egs.sergey.mytestapp.bus;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

public class AndroidBus extends Bus {
    private final Handler mainThread = new Handler(Looper.getMainLooper());

    private static final AndroidBus instance = new AndroidBus();

    private AndroidBus() {
        // To be able post events from all threads.
        super(ThreadEnforcer.ANY);
    }

    public static AndroidBus getInstance() {
        return instance;
    }

    @Override
    public void post(final Object event) {
        // To be able automatically handle events in main thread which were posted from background thread
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event);
        } else {
            mainThread.post(new Runnable() {
                @Override
                public void run() {
                    post(event);
                }
            });
        }
    }
}
