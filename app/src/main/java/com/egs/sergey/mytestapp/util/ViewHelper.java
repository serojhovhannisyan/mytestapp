package com.egs.sergey.mytestapp.util;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.egs.sergey.mytestapp.R;

public class ViewHelper {

    /**
     *
     * @param context the context from where method were called
     * @param view the view inside which message will be displayed
     * @param errorMessage the concrete message to be display
     * @param isActionEnabled does action button should be displayed
     */
    public static void showSnackbar(final Context context, View view, String errorMessage, boolean isActionEnabled) {
        final Snackbar snackbar = Snackbar.make(view, errorMessage, Snackbar.LENGTH_LONG);

        if (isActionEnabled) {
            snackbar.setAction(R.string.snackbar_action_settings, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            });
        }

        snackbar.show();
    }
}
