package com.egs.sergey.mytestapp.service;

import android.content.Context;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.egs.sergey.mytestapp.R;
import com.egs.sergey.mytestapp.api.ApiClient;
import com.egs.sergey.mytestapp.api.ApiManager;
import com.egs.sergey.mytestapp.model.Point;
import com.egs.sergey.mytestapp.model.PointDetail;
import com.egs.sergey.mytestapp.model.PointList;
import com.egs.sergey.mytestapp.event.PointDetailLoadedEvent;
import com.egs.sergey.mytestapp.event.LoadPointDetailEvent;
import com.egs.sergey.mytestapp.event.LoadPointsEvent;
import com.egs.sergey.mytestapp.event.PointsLoadedEvent;
import com.egs.sergey.mytestapp.util.NetworkHelper;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PoiService {

    private Context context;
    private Bus bus;
    private ApiClient apiClient;

    public PoiService(Context context, Bus bus) {
        this.context = context;
        this.bus = bus;
        apiClient = ApiManager.getClient();
    }

    @Subscribe
    public void onLoadPointsEvent(LoadPointsEvent event) {
        // if there is no connection or something happened while fetching json then get data from db.
        if (NetworkHelper.getConnectivityStatus(context) != NetworkHelper.TYPE_NOT_CONNECTED) {
            apiClient.getPoints(new Callback<PointList>() {
                @Override
                public void success(PointList pointList, Response response) {
                    List<Point> points = pointList.getList();
                    bus.post(new PointsLoadedEvent(points));
                    savePointsIntoDatabase(points);
                }

                @Override
                public void failure(RetrofitError error) {
                    readPointsFromDatabaseAndSendBackToView(context.getString(R.string.remote_server_error));
                }
            });
        } else {
            readPointsFromDatabaseAndSendBackToView(context.getString(R.string.phone_connection_error));
        }
    }

    @Subscribe
    public void onLoadPointDetailEvent(final LoadPointDetailEvent event) {
        // if there is no connection or something happened while fetching json then get data from db.
        if (NetworkHelper.getConnectivityStatus(context) != NetworkHelper.TYPE_NOT_CONNECTED) {
            apiClient.getDetailPoint(event.getId(), new Callback<PointDetail>() {
                @Override
                public void success(PointDetail pointDetail, Response response) {
                    bus.post(new PointDetailLoadedEvent(pointDetail));
                    savePointDetailIntoDatabase(pointDetail);
                }

                @Override
                public void failure(RetrofitError error) {
                    readPointDetailFromDatabaseAndSendBackToView(event.getId(), context.getString(R.string.remote_server_error));
                }
            });
        } else {
            readPointDetailFromDatabaseAndSendBackToView(event.getId(), context.getString(R.string.phone_connection_error));
        }
    }

    /**
     * Reads list of Points from database and send it to appropriate subscriber using event bus mechanism.
     * The method runs in separate thread.
     * @param errorMessage error message which should be sent with event
     */
    private void readPointsFromDatabaseAndSendBackToView(final String errorMessage) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Point> list = readPointsFromDatabase();
                PointsLoadedEvent pointsLoadedEvent = new PointsLoadedEvent(list);
                pointsLoadedEvent.setIsError(true);
                pointsLoadedEvent.setErrorMessage(errorMessage);
                bus.post(pointsLoadedEvent);
            }
        }).start();

    }

    /**
     * Reads single PointDetails from database and send it to appropriate subscriber using event bus mechanism.
     * The method runs in separate thread.
     * @param id ID of the Point
     * @param errorMessage error message which should be sent with event
     */
    private void readPointDetailFromDatabaseAndSendBackToView(final Long id, final String errorMessage) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                PointDetail pointDetail = readPointDetailFromDatabase(id);
                PointDetailLoadedEvent pointDetailLoadedEvent = new PointDetailLoadedEvent(pointDetail);
                pointDetailLoadedEvent.setIsError(true);
                pointDetailLoadedEvent.setErrorMessage(errorMessage);
                bus.post(pointDetailLoadedEvent);
            }
        }).start();

    }

    /**
     * Save list of Points into db. The data will be updated if it's already exist.
     * It use one transaction for all the list
     * @param points list of Points
     */
    private void savePointsIntoDatabase(final List<Point> points) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ActiveAndroid.beginTransaction();
                try {
                    for (Point point : points) {
                        point.save();
                    }
                    ActiveAndroid.setTransactionSuccessful();
                }
                finally {
                    ActiveAndroid.endTransaction();
                }
            }
        }).start();
    }

    // Save fetched data into db. The data will be updated if it's already exists

    /**
     * Save Point into db. The data will be updated if it's already exists.
     * @param pointDetail the PointDetail which should be saved
     */
    private void savePointDetailIntoDatabase(final PointDetail pointDetail) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                pointDetail.save();
            }
        }).start();
    }

    /**
     * Get all points from db
     * @return the list of Points
     */
    private List<Point> readPointsFromDatabase() {
        return new Select()
                .from(Point.class)
                        .execute();
    }

    /**
     * Get single Point object from db
     * @param id the id of Point
     * @return single PointDetail
     */
    private PointDetail readPointDetailFromDatabase(Long id) {
        return new Select()
                .from(PointDetail.class)
                .where("Poi_id =" + id)
                .executeSingle();
    }
}
